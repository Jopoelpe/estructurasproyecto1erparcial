/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package List;

/**
 *
 * @author Jopoe
 * @param <E>
 */
public class DoublyNode<E> {
    private DoublyNode<E> next = null;
    private DoublyNode<E> previous = null;
    private E content;
    //constructors
    public DoublyNode(){
        content = null;
    }
    //We'll rather choose this one
    public DoublyNode(E content){
        this.content = content;
    }
    //Methods
    public DoublyNode<E> getNext() {
        return next;
    }

    public void setNext(DoublyNode<E> next) {
        this.next = next;
    }

    public DoublyNode<E> getPrevious() {
        return previous;
    }

    public void setPrevious(DoublyNode<E> previous) {
        this.previous = previous;
    }

    public E getContent() {
        return content;
    }

    public void setContent(E content) {
        this.content = content;
    }
    @Override
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(content);
        return s.toString();
    }
}
