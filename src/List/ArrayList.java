/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package List;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 *
 * @author Jopoe
 * @param <E>
 */
public class ArrayList<E> implements List<E>{
    private E elements[];
    private int capacity;
    private int effective;
    public ArrayList(){
        this.elements = (E[]) new Object[10];
        this.capacity = 10;
        this.effective = 0;  
    }
    
    private void addCapacity(){
        E extendedArray[] = (E[]) new Object[capacity*2];
        for(int tmp = 0; tmp < capacity; tmp++){
            extendedArray[tmp] = elements[tmp];
        }
        elements = extendedArray;
        capacity *= 2;
    }
    @Override
    public boolean addFirst(E e) {
        if(e == null){
            return false;
        }
        else if(this.isEmpty()){
            elements[0] = e;
            effective++;
            return true;
        }else if(capacity == effective){
            addCapacity();
        }
        for(int tmp = effective; tmp > 0; tmp--){
            elements[tmp] = elements[tmp-1];
        }
        elements[0] = e;
        effective++;
        return true;
        
            
    }
            
    

    @Override
    public boolean addLast(E e) {
        if(e == null){
            return false;
        }else if(this.isEmpty()){
            elements[0] = e;
            effective++;
            return true;
        }else if(effective == capacity){
            addCapacity();
        }
            elements[effective] = e;
            effective++;
            return true;
        
    }

    /*public void add(int index, E element) {
        if(element == null)
            throw new NullPointerException();
        else if(index > effective || index < 0)
            throw new IndexOutOfBoundsException();
        else{
            if(effective == capacity)
                addCapacity();
            for(int i = effective; i > index; i--)
               elements[effective] = elements[effective-1];
            elements[index] = element;
            effective++;
        }
    }
    */

    @Override
    public E remove(int index) {
        if(index >= effective || index < 0)
            return null;
        E tempAr[] = (E[]) new Object[capacity];
        E tempEl = elements[index];
        for(int i = 0; i < (effective-1); i++){
            if(i < index)
                tempAr[i] = elements[i];
            else
                tempAr[i] = elements[i+1];
        }
        elements = tempAr;
        effective--;
        return tempEl;
    }
    

    @Override
    public E get(int index) {
        if(index >= effective || index < 0){
            return null;
        }else
            return elements[index];
    }
/*
    @Override
    public E set(int index, E element) {
        if(elements == null){
            return null;
        }else if(index >= capacity || index < 0){
            return null;
        }else{
            elements[index] = element;
            return element;
        }
    }
    */

    @Override
    public int size() {
        return effective;
    }

    @Override
    public boolean isEmpty() {
        return (effective == 0);
    }
    /*
    @Override
    public void clear() {
        E clearedArray[] = (E[]) new Object[10];
        elements = clearedArray;
        effective = 0;
        capacity = 10;
    }
*/
    @Override
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append('[');
        for(int i = 0; i < (effective-1); i++){
            s.append(elements[i]);
            s.append(',');
            s.append(' ');
        }
        s.append(elements[effective-1]);
        s.append(']');
        return s.toString();
    }

    @Override
    public E removeFirst() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public E removeLast() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void rotate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean add(int index, E content) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterator<E> iterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
