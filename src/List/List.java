/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package List;

/**
 *
 * @author Jopoe
 * @param <E>
 */
public interface List<E> extends Iterable<E>{
    public boolean addFirst(E content);// throws NullPointerException; // Adds an element to the first position in List
    public boolean addLast(E content); //throws NullPointerException; //Appends an element to the end of the List
    public boolean add(int index, E content);// throws IndexOutOfBoundsException, NullPointerException; //Adds an element in a specific index
    public E get(int index);
    public E removeFirst();// throws NullPointerException;
    public E removeLast();// throws NullPointerException;
    public E remove(int index);// throws IndexOutOfBoundsException //Removes an element in a specific index
    public void rotate();// throws NullPointerException; //Moves the first element to the end of the list
    public boolean isEmpty(); //Returns if the list in empty
    public int size(); //Returns the size of the list
}
