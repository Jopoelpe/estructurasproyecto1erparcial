/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package List;

import java.util.Iterator;
import java.util.ListIterator;

/**
 *
 * @author Jopoe
 * @param <E>
 */
public class CircularDoublyLinkedList<E> implements List<E>{
    private DoublyNode<E> last;
    private int effective;
    public CircularDoublyLinkedList(){
        last = null;
        effective = 0;
    }

    @Override
    public boolean addFirst(E content) {
        if(content == null)
            return false;
        else if(this.isEmpty()){
            last = new DoublyNode(content);
            last.setNext(last);
            last.setPrevious(last);
        }else{
            DoublyNode<E> tmp = new DoublyNode(content);
            tmp.setNext(last.getNext());
            tmp.setPrevious(last);
            last.setNext(tmp);
        }  
        effective++;
        return true;
    }

    @Override
    public boolean addLast(E content) {
        if(content == null)
            return false;
        else if(this.isEmpty()){
            last = new DoublyNode(content);
            last.setNext(last);
            last.setPrevious(last);
        }else{
            DoublyNode<E> tmp = new DoublyNode(content);
            last.getNext().setPrevious(tmp);
            tmp.setNext(last.getNext());
            tmp.setPrevious(last);
            last.setNext(tmp);
            last = tmp;
        }
        effective++;
        return true;
    }

    @Override
    public boolean add(int index, E content) {
        if(index > effective)
            return false;
        else if(content == null)
            return false;
        else if(index == effective)
            return addLast(content);
        DoublyNode<E> travelerNode = last.getNext();
        DoublyNode<E> newNode = new DoublyNode(content);
        for(int iter = 0; iter < index; iter++)
            travelerNode = travelerNode.getNext();
        travelerNode.getPrevious().setNext(newNode);
        newNode.setPrevious(travelerNode.getPrevious());
        travelerNode.setPrevious(newNode);
        newNode.setNext(travelerNode);
        effective++;
        return true;
    }
    @Override
    public E get(int index) {
        if(this.isEmpty())
            return null;
        else if(index >= effective || index < 0)
            return null;
        DoublyNode<E> travelerNode = last.getNext();
        for(int iter = 0; iter < index; iter ++)
            travelerNode = travelerNode.getNext();
        return travelerNode.getContent();
    }
    

    @Override
    public E removeFirst() {
        if(this.isEmpty())
            return null;
        else if(effective == 1){
            DoublyNode<E> tmp = last;
            last = null;
            effective--;
            return tmp.getContent();
        }
        DoublyNode<E> first = last.getNext();
        first.getPrevious().setNext(first.getNext());
        first.getNext().setPrevious(first.getPrevious());
        effective--;
        return first.getContent();
    }

    @Override
    public E removeLast() {
        if(this.isEmpty())
            return null;
        else if(effective == 1){
            DoublyNode<E> tmp = last;
            last = null;
            effective--;
            return tmp.getContent();
        }
        DoublyNode<E> tmp = last;
        tmp.getPrevious().setNext(tmp.getNext());
        tmp.getNext().setPrevious(tmp.getPrevious());
        last = tmp.getPrevious();
        effective--;
        return tmp.getContent();
    }

    @Override
    public E remove(int index) {
        if(index >= effective || index < 0)
            return null;
        else if(index == (effective-1)){ //It's necessary the method removeLast, because we could lose the "last" reference and will throw an Exception
            return this.removeLast();
        }
        int iter;
        DoublyNode<E> travelerNode = last.getNext(); //Gets the first Node
        for(iter = 0; iter < index; iter++){
            travelerNode = travelerNode.getNext();
        }
        travelerNode.getPrevious().setNext(travelerNode.getNext());
        travelerNode.getNext().setPrevious(travelerNode.getPrevious());
        effective--;
        return travelerNode.getContent();
    }

    @Override
    public void rotate() {
        if(last != null)
            last = last.getNext();
    }

    @Override
    public boolean isEmpty() {
        return effective == 0;
    }

    @Override
    public int size() {
        return effective;
    }
    
    @Override
    public Iterator<E> iterator() {
        Iterator<E> it = new Iterator(){
            DoublyNode<E> travelerNode = last;
            int count = effective;
            @Override
            public boolean hasNext() {
                return count != 0;
            }

            @Override
            public E next() {
                travelerNode = travelerNode.getNext();
                count--;
                return travelerNode.getContent();
            }
            
        };
        return it;
    }
    
    public ListIterator<E> listIterator(){
        ListIterator<E> iterList = new ListIterator(){
            DoublyNode<E> travelerNode = last;
            @Override
            public boolean hasNext() {
                return effective != 0;
            }

            @Override
            public Object next() {
                travelerNode = travelerNode.getNext();
                return travelerNode.getContent();
            }

            @Override
            public boolean hasPrevious() {
                return effective != 0;
            }

            @Override
            public Object previous() {
                DoublyNode<E> tmp = travelerNode;
                travelerNode = travelerNode.getPrevious();
                return tmp.getContent();
            }

            @Override
            public int nextIndex() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public int previousIndex() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void set(Object e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void add(Object e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
            
        };
        return iterList;
    }
    
    @Override
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append("[");
        if(!this.isEmpty()){
            DoublyNode<E> travelerNode;
            for(travelerNode = last.getNext(); travelerNode != last; travelerNode = travelerNode.getNext()){
                s.append(travelerNode.getContent());
                s.append(", ");
            }
            s.append(last.getContent());
        }
        s.append("]");
        return s.toString();
    }

    public DoublyNode<E> getLast() {
        return last;
    }
    
    public void clear(){
        getFirst().setPrevious(null);
        getFirst().setNext(null);
        getFirst().setContent(null);
        effective = 0;
    }
    
    public DoublyNode<E> getFirst(){
        return last.getNext();
    }

    public void setLast(DoublyNode<E> last) {
        this.last = last;
    }
}
