/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import List.ArrayList;
import List.CircularDoublyLinkedList;
import List.DoublyNode;
import Logica.LogicaJuego;
import Soldado.Soldado;
import java.util.ListIterator;
import java.util.Map;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 *
 * @author Jopoe
 */
public class VentanaPrincipal {
    private Group root = new Group();
    private StackPane stack = new StackPane();
    private Rectangle rectangle = new Rectangle();
    private Text error = new Text();
    private Circle circle = new Circle();
    private TextField numSoldadosField = new TextField();
    private Text title = new Text("Joseph Simulator");
    private Text advice = new Text("Escoja el numero de soldados");
    private Button start = new Button("Empezar simulacion");
    private Map<Integer, ArrayList<Double>> mapSizes;
    private boolean isSimulating = false;
    private ToggleGroup group = new ToggleGroup();
    private RadioButton horario = new RadioButton();
    private RadioButton antihorario = new RadioButton();
    private Text textHorario = new Text("Horario");
    private Text textAntiHorario = new Text("AntiHorario");
    CircularDoublyLinkedList<Soldado> soldados;
    Thread t;
    public VentanaPrincipal(){
        mapSizes = LogicaJuego.getResizeLocationMap();
        organizar();
    }
    
    public void organizar(){
        title.setFont(new Font(25));        //Se acomodan detalles sin incluir los soldados
        title.setX(230);
        title.setY(60.0);
        title.setUnderline(true);
        
        advice.setFont(new Font(20.0));
        advice.setX(610);
        advice.setY(110);
        
        numSoldadosField.setPrefWidth(60);
        numSoldadosField.setFont(new Font(20));
        numSoldadosField.setLayoutX(710);
        numSoldadosField.setLayoutY(130);
        
        error.setFill(Color.RED);
        error.setLayoutX(655);
        error.setLayoutY(200);
        
        start.setLayoutX(680);
        start.setLayoutY(260);
        start.setScaleX(1.5);
        start.setScaleY(1.5);
        stack.getChildren().addAll(rectangle, circle);
        stack.setLayoutX(10);
        stack.setLayoutY(90);
        start.setOnMouseClicked(e-> empezarSimulacion()); //Evento que inicia la simulación
        
        horario.setToggleGroup(group);
        antihorario.setToggleGroup(group);
        horario.setSelected(true);
        antihorario.setLayoutX(680);
        antihorario.setLayoutY(200);
        horario.setLayoutX(780);
        horario.setLayoutY(200);
        
        textAntiHorario.setLayoutX(650);
        textAntiHorario.setLayoutY(235);
        textHorario.setLayoutX(770);
        textHorario.setLayoutY(235);
        
        rectangle.setHeight(510);
        rectangle.setWidth(580);
        rectangle.setFill(Color.rgb(88, 241, 95));
        
        circle.setRadius(200);
        circle.setFill(Color.rgb(88, 241, 95));
        circle.setStroke(Color.BLACK);
        
        root.getChildren().addAll(title, advice, numSoldadosField, error, start, stack, horario, antihorario, textHorario, textAntiHorario);
        
    }
    public Group getRoot(){
        return root;
    }

    private void empezarSimulacion() {
        
        if(numSoldadosField.getText().length() == 0)    //Validacion de entradas en el textbox
            error.setText("Por favor, escriba un numero");
        else if(!isDigitString(numSoldadosField.getText()))
            error.setText("Entrada incorrecta, escriba un numero");
        else{
            int num = Integer.parseInt(numSoldadosField.getText());
            if(num < 6 | num > 26)
                error.setText("Ingrese un numero entre 6 - 26");
            else{
                if(isSimulating){                                                //Ayuda a reiniciar la simulacion en tiempo de ejecucion
                    root.getChildren().remove(10, root.getChildren().size());
                    t.interrupt();
                }
                error.setText("");
                isSimulating = true;
                int indiceFlavio = LogicaJuego.obtenerIndiceFlavio(num);
                if(horario.isSelected())
                    soldados = LogicaJuego.cargarListaSoldadosHorario(num, indiceFlavio);  //Se cargan los n soldados en una lista circulardoblemente enlazada
                else
                    soldados = LogicaJuego.cargarListaSoldadosAntiHorario(num, indiceFlavio);
                cargarSoldados(soldados);                                       //Muestra dichos soldados en pantalla
                t = new Thread(new HiloSoldados());                      //Este hijo es importante, ya que practicamente controla toda la simulacion
                t.start();
                t.interrupt();
                }
            
        }
    }
    
    public void cargarSoldados(CircularDoublyLinkedList<Soldado> list){
        double radianes = (360.0/list.size())*(Math.PI/180);
        double radianesCorregido = radianes + radianes*(mapSizes.get(list.size()).get(0));  //Este es un factor de corrección en un mapa en la clase LogicaJuego, sirve para poner al primer soldado a las 12 en punto
        double angleX = Math.cos(radianesCorregido);    //Se transforma en coordenadas polares, ya que se trata de ubicarlo en el círculo que se presenta
        double angleY = Math.sin(radianesCorregido);
        DoublyNode<Soldado> sold = list.getFirst();
        for(int i = 0; i < list.size(); i++){
            ImageView image = new ImageView(sold.getContent().getImage());
            image.setFitHeight(mapSizes.get(list.size()).get(1));
            image.setFitWidth(mapSizes.get(list.size()).get(2));
            double axisX = (200.0)*angleX;
            double axisY = (200.0)*angleY;
            image.setLayoutX(mapSizes.get(list.size()).get(3) + axisX);
            image.setLayoutY(mapSizes.get(list.size()).get(4) + axisY);
            radianesCorregido += radianes;      //Importante sumar los radianes calculados primero y no el corregido, el corregido es solo para el primer soldado a las 12 en punto
            angleX = Math.cos(radianesCorregido);       //se actualiza el nuevo ángulo
            angleY = Math.sin(radianesCorregido);
            root.getChildren().add(image);
            sold = sold.getNext();  //Actualiza el soldado
        }
    }
    
    
    
    public boolean isDigitString(String text){  //Valida si la entrada es un digito
        char[] array = text.toCharArray();
        for(char c: array){
            if(!Character.isDigit(c)){
                return false;
            }
        }
        return true;
    }


    public class HiloSoldados implements Runnable{
        int iter;           //Importante declararlas en esta clase interna, no deja declarar en el run
        int vivos = soldados.size();
        boolean matar = false;
        ListIterator<Soldado> it = soldados.listIterator();
        @Override
        public void run() {
            if(horario.isSelected())
                iter = 10;
            else
                iter = root.getChildren().size();
            while(vivos > 1){
                try{
                    Platform.runLater(new Runnable(){
                    public void run(){
                           //Se recorre la lista observable del root que contiene las imageView a la par con la lista de soldados
                        if(horario.isSelected()){
                            Soldado next = it.next();
                            matarSoldadoHorario(next);
                        }else{
                            Soldado previous = it.previous();
                            matarSoldadoAntiHorario(previous);
                        }
                    }
                    });
                    Thread.sleep(500);
                } catch(InterruptedException e){}
            }
        }
        public void matarSoldadoHorario(Soldado sold){ 
            if(!matar){
                if(!sold.isMuerto()) //Validacion importante en la segunda vuelta
                    matar = true;
            }else if(matar){
                if(!sold.isMuerto()){
                    ImageView image = new ImageView(new Image(LogicaJuego.muertePath));
                    double radianes = (360.0/soldados.size())*(Math.PI/180);
                    double radianesCorregido = (radianes + radianes*(mapSizes.get(soldados.size()).get(0)))*(iter-5);
                    double angleX = Math.cos(radianesCorregido);
                    double angleY = Math.sin(radianesCorregido);
                    image.setFitHeight(mapSizes.get(soldados.size()).get(1));
                    image.setFitWidth(mapSizes.get(soldados.size()).get(2));
                    image.setLayoutX(root.getChildren().get(iter).getLayoutX());     //Obtiene las posiciones exactas de la imagen para luego ser reemplazada
                    image.setLayoutY(root.getChildren().get(iter).getLayoutY());
                    root.getChildren().set(iter, image);
                    matar = false;
                    sold.setMuerto(true); //Propiedad importante de la clase soldado.
                    vivos--;
                }
            }
            if(iter == (root.getChildren().size()) - 1)     //Validacion importante ya que la ObservableList no es una lista circular, tiene que regresar al origen con índices.
                iter = 10;
            else if(iter < (root.getChildren().size()-1))
                iter++;
        }
        
        public void matarSoldadoAntiHorario(Soldado sold){ 
            if(!matar){
                if(!sold.isMuerto()) //Validacion importante en la segunda vuelta
                    matar = true;
            }else if(matar){
                if(!sold.isMuerto()){
                    ImageView image = new ImageView(new Image(LogicaJuego.muertePath));
                    double radianes = (360.0/soldados.size())*(Math.PI/180);
                    double radianesCorregido = (radianes + radianes*(mapSizes.get(soldados.size()).get(0)))*(iter-5);
                    double angleX = Math.cos(radianesCorregido);
                    double angleY = Math.sin(radianesCorregido);
                    image.setFitHeight(mapSizes.get(soldados.size()).get(1));
                    image.setFitWidth(mapSizes.get(soldados.size()).get(2));
                    image.setLayoutX(root.getChildren().get(iter).getLayoutX());     //Obtiene las posiciones exactas de la imagen para luego ser reemplazada
                    image.setLayoutY(root.getChildren().get(iter).getLayoutY());
                    root.getChildren().set(iter, image);
                    matar = false;
                    sold.setMuerto(true); //Propiedad importante de la clase soldado.
                    vivos--;
                }
            }
            if(iter == 10)     //Validacion importante ya que la ObservableList no es una lista circular, tiene que regresar al origen con índices.
                iter = (root.getChildren().size()) - 1;
            else if(iter > 10)
                iter--;
        }
    }
}

    
