package Soldado;

import javafx.scene.image.Image;

/**
 *
 * @author ronny
 */
public class Soldado  {
    private boolean muerto;
    private boolean flavio;
    Image image;
    
    /*
    Constructor del soldado
    */
    public Soldado(boolean flavio, Image image) {
        this.muerto = false;
        this.flavio = flavio;
        this.image = image;
    }

    public boolean isMuerto() {
        return muerto;
    }

    public boolean isFlavio() {
        return flavio;
    }

    public Image getImage() {
        return image;
    }

    public void setMuerto(boolean muerto) {
        this.muerto = muerto;
    }

    public void setFlavio(boolean flavio) {
        this.flavio = flavio;
    }

    public void setImage(Image image) {
        this.image = image;
    }
    public String toString(){
        return "Soldado";
    }
}
