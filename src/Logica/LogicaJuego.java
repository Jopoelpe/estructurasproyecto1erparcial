/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import List.ArrayList;
import List.CircularDoublyLinkedList;
import Soldado.Soldado;
import java.util.HashMap;
import java.util.Map;
import javafx.scene.image.Image;

/**
 *
 * @author Jopoe
 */
public class LogicaJuego {
    private final static int potencias[] = {64, 32, 16, 8, 4, 2, 1};
    public static String soldadoPath = "file:src/Soldado/soldado.png";
    public static String flavioPath = "file:src/Soldado/flavio.png";
    public static String muertePath = "file:src/Soldado/muerte.png";
    public static int obtenerIndiceFlavio(int numSoldados){
        int resto = obtenerRestoPotencia(numSoldados);
        return resto*2; //La formula original seria "resto*2 + 1" pero como son indices, no se le suma uno.
    }

    private static int obtenerRestoPotencia(int num) { //Importante para el índice de Flavio, ya que se basa en potencias de 2.
        for(int iter = 0; iter < potencias.length; iter++){
            if(potencias[iter] <= num)
                return (num - potencias[iter]);
        }
        return -1;
    }
    
    public static CircularDoublyLinkedList<Soldado> cargarListaSoldadosHorario(int num, int indiceFlavio) {
        CircularDoublyLinkedList<Soldado> temp = new CircularDoublyLinkedList<>();
        for(int iter = 0; iter < num; iter++){
            if(iter == indiceFlavio){
                temp.addLast(new Soldado(true, new Image(flavioPath)));
            }else
                temp.addLast(new Soldado(false, new Image(soldadoPath)));
        }
        return temp;
    }
    
    public static CircularDoublyLinkedList<Soldado> cargarListaSoldadosAntiHorario(int num, int indiceFlavio) {
        CircularDoublyLinkedList<Soldado> temp = new CircularDoublyLinkedList<>();
        for(int iter = 0; iter < num; iter++){
            if(iter == (num - indiceFlavio)){
                temp.addLast(new Soldado(true, new Image(flavioPath)));
            }else if(iter == 0 && indiceFlavio == 0){
                temp.addLast(new Soldado(true, new Image(flavioPath)));
            }else
                temp.addLast(new Soldado(false, new Image(soldadoPath)));
        }
        return temp;
    }
    
    public static Map<Integer, ArrayList<Double>> getResizeLocationMap(){
        Map<Integer, ArrayList<Double>> temp = new HashMap<>();
        //Esta función crea y devuelve un mapa con los datos que calculamos sobre las posiciones de los soldados
        ArrayList<Double> sizesOne= new ArrayList();
        sizesOne.addLast(100.0);
        sizesOne.addLast(100.0);
        sizesOne.addLast(250.0);
        sizesOne.addLast(290.0);
        ////
        ArrayList<Double> filaUno = new ArrayList(); 
        filaUno.addLast(3.5);
        addAll(filaUno, sizesOne);
        
        ArrayList<Double> filaDos = new ArrayList(); 
        filaDos.addLast(4.3);
        addAll(filaDos, sizesOne);
        
        ArrayList<Double> filaTres = new ArrayList(); 
        filaTres.addLast(5.0);
        addAll(filaTres, sizesOne);
        
        ArrayList<Double> filaCuatro= new ArrayList(); 
        filaCuatro.addLast(5.8);
        addAll(filaCuatro, sizesOne);
        
        ArrayList<Double> filaCinco = new ArrayList(); 
        filaCinco.addLast(6.6);
        addAll(filaCinco, sizesOne);
        
        ArrayList<Double> filaSeis = new ArrayList(); 
        filaSeis.addLast(7.3);
        addAll(filaSeis, sizesOne);
        
        ArrayList<Double> filaSiete = new ArrayList(); 
        filaSiete.addLast(8.1);
        addAll(filaSiete, sizesOne);
        
        ArrayList<Double> filaOcho = new ArrayList(); 
        filaOcho.addLast(8.8);
        addAll(filaOcho, sizesOne);
        /////
        ArrayList<Double> sizesTwo = new ArrayList();
        sizesTwo.addLast(60.0);
        sizesTwo.addLast(60.0);
        sizesTwo.addLast(270.0);
        sizesTwo.addLast(310.0);
        //
        ArrayList<Double> filaNueve = new ArrayList(); 
        filaNueve.addLast(9.6);
        addAll(filaNueve, sizesTwo);
        
        ArrayList<Double> filaDiez = new ArrayList(); 
        filaDiez.addLast(10.4);
        addAll(filaDiez, sizesTwo);
        
        ArrayList<Double> filaOnce = new ArrayList(); 
        filaOnce.addLast(11.1);
        addAll(filaOnce, sizesTwo);
        
        ArrayList<Double> filaDoce = new ArrayList(); 
        filaDoce.addLast(11.9);
        addAll(filaDoce, sizesTwo);
        
        ArrayList<Double> filaTrece = new ArrayList(); 
        filaTrece.addLast(12.7);
        addAll(filaTrece, sizesTwo);
        
        ArrayList<Double> filaCatorce = new ArrayList(); 
        filaCatorce.addLast(13.5);
        addAll(filaCatorce, sizesTwo);
        
        ArrayList<Double> filaQuince = new ArrayList(); 
        filaQuince.addLast(14.2);
        addAll(filaQuince, sizesTwo);
        
        ArrayList<Double> filaDieciseis = new ArrayList(); 
        filaDieciseis.addLast(15.0);
        addAll(filaDieciseis, sizesTwo);
        /////
        ArrayList<Double> sizesThree = new ArrayList();
        sizesThree.addLast(40.0);
        sizesThree.addLast(40.0);
        sizesThree.addLast(280.0);
        sizesThree.addLast(320.0);
        /////
        ArrayList<Double> filaDiecisiete = new ArrayList(); 
        filaDiecisiete.addLast(15.7);
        addAll(filaDiecisiete, sizesThree);
        
        ArrayList<Double> filaDieciocho = new ArrayList(); 
        filaDieciocho.addLast(16.5);
        addAll(filaDieciocho, sizesThree);
        
        ArrayList<Double> filaDiecinueve = new ArrayList(); 
        filaDiecinueve.addLast(17.2);
        addAll(filaDiecinueve, sizesThree);
        
        ArrayList<Double> filaVeinte = new ArrayList(); 
        filaVeinte.addLast(17.9);
        addAll(filaVeinte, sizesThree);
        
        ArrayList<Double> filaVeintiuno = new ArrayList(); 
        filaVeintiuno.addLast(18.7);
        addAll(filaVeintiuno, sizesThree);
        ////
        temp.put(6, filaUno);
        temp.put(7, filaDos);
        temp.put(8, filaTres);
        temp.put(9, filaCuatro);
        temp.put(10, filaCinco);
        temp.put(11, filaSeis);
        temp.put(12, filaSiete);
        temp.put(13, filaOcho);
        temp.put(14, filaNueve);
        temp.put(15, filaDiez);
        temp.put(16, filaOnce);
        temp.put(17, filaDoce);
        temp.put(18, filaTrece);
        temp.put(19, filaCatorce);
        temp.put(20, filaQuince);
        temp.put(21, filaDieciseis);
        temp.put(22, filaDiecisiete);
        temp.put(23, filaDieciocho);
        temp.put(24, filaDiecinueve);
        temp.put(25, filaVeinte);
        temp.put(26, filaVeintiuno);
        
        return temp;
    }
    public static void main(String[] args) {
        System.out.println(obtenerIndiceFlavio(41));
    }
    
    public static void addAll(ArrayList<Double> returnList, ArrayList<Double> list){
        for(int i = 0; i < list.size(); i++)
            returnList.addLast(list.get(i));
    }
}
