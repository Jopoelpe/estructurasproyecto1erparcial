/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Ventanas.VentanaPrincipal;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author Jopoe
 */
public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage stage) throws Exception {
        Scene scene = new Scene(new VentanaPrincipal().getRoot(), 900, 600);
        scene.setFill(Color.rgb(54, 204, 228));
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle("Joseph Flavio");
        stage.show();
    }
    
}
